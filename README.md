# Application Summary

## Quick Start

### Prerequisites

* [Java 8 JDK or higher](https://openjdk.java.net/install/)

### Start Service Steps in local
1. Find source code from [BitBucket](https://bitbucket.org/kasimura/acmebank_account-manager/src/master/)
2. Choose 'Download Repository' and save it to anywhere you want
3. unzip the zip file download
4. Change directory to accounts in command prompt
5. On MacOS/Linux `./mvnw spring-boot:run` Or On Windows `.\mvnw.cmd spring-boot:run` 

## Endpoints
* http://localhost:8080/account-manager/v1/balance/{account_id} (GET)
* http://localhost:8080/account-manager/v1/transfer (POST with below payload body as example)

```
{
   "payerAccountId": 88888888,
   "payeeAccountId": 12345678,
   "amount": {
       "currencyCode": "HKD",
       "amount": 1000.00
   }
}
```

## Swagger OAS v3 API Documentation
* [Account Manager OpenAPI Documentation](http://localhost:8080/account-manager/v1/swagger-ui.html)

## Assumptions
* Assumed unit test should be covered by developer and only business logic.
* Assumed database are shared by pods of same microservices.
* Assumed the account balance could not be negative.
* Assumed same account transaction could not be made.
* Assumed the H2 database file stored in user directory. Please change ```spring.jpa.hiberate.ddl-auto=create``` in ```src/main/resources/application.yaml``` for database initiation at first run. 
