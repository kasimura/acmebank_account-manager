package com.acmebank.account_manager.controllers;

import com.acmebank.account_manager.data.entities.Account;
import com.acmebank.account_manager.data.models.TransactionRequest;
import com.acmebank.account_manager.data.models.TransactionResponse;
import com.acmebank.account_manager.services.AccountManager;
import com.acmebank.account_manager.utils.Constants;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {
    @Autowired
    private AccountManager accountManager;

    @ApiResponse(responseCode = "200", description = "Success!")
    @ApiResponse(responseCode = "400", description = "Invalid Input(s)!", content = @Content(mediaType = "application/json",schema = @Schema(implementation = Exception.class)))
    @GetMapping(Constants.BALANCE_ENDPOINT)
    public ResponseEntity<Account> balance(@PathVariable String id) {
        return ResponseEntity.ok(accountManager.getBalance(Long.valueOf(id)).orElseThrow(RuntimeException::new));
    }

    @ApiResponse(responseCode = "201", description = "Created!")
    @ApiResponse(responseCode = "400", description = "Invalid Input(s)!", content = @Content(mediaType = "application/json",schema = @Schema(implementation = Exception.class)))
    @PostMapping(Constants.TRANSFER_ENDPOINT)
    public ResponseEntity<TransactionResponse> transfer(@RequestBody TransactionRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(accountManager.transfer(request));
    }

    @ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Invalid input(s)!")
    @ExceptionHandler(Exception.class)
    public void exceptionHandler() { }
}
