package com.acmebank.account_manager.services;

import com.acmebank.account_manager.data.entities.Account;
import com.acmebank.account_manager.data.models.Amount;
import com.acmebank.account_manager.data.models.TransactionRequest;
import com.acmebank.account_manager.data.models.TransactionResponse;
import com.acmebank.account_manager.data.repositories.AccountRepository;
import com.acmebank.account_manager.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

@Component
public class AccountManager {
    @Autowired
    private AccountRepository accountRepository;

    @Transactional(readOnly = true, timeout = 2)
    public Optional<Account> getBalance(Long accountId) {
        return accountRepository.findById(accountId);
    }

    @Transactional(timeout = 2)
    public TransactionResponse transfer(TransactionRequest request) {
        if(request.getPayeeAccountId().equals(request.getPayerAccountId())) {
            throw new IllegalArgumentException(Constants.EXCEPTION_MESSAGE_SAME_ACCOUNT_ID);
        }
        Optional<Account> payerAccountOptional = accountRepository.findById(request.getPayerAccountId());
        Optional<Account> payeeAccountOptional = accountRepository.findById(request.getPayeeAccountId());
        Account payerAccount;
        Account payeeAccount;
        BigDecimal amountTransfer;
        if(!Constants.DEFAULT_CURRENCY_CODE.equalsIgnoreCase(request.getAmount().getCurrencyCode())){
            throw new IllegalArgumentException(Constants.EXCEPTION_MESSAGE_INVALID_CURRENCY_CODE);
        }
        if(!payerAccountOptional.isPresent() || !payeeAccountOptional.isPresent()) {
            throw new IllegalArgumentException(Constants.EXCEPTION_MESSAGE_INVALID_PAYER_OR_PAYEE);
        } else {
            amountTransfer = request.getAmount().getAmount();
            payerAccount = payerAccountOptional.get();
            payeeAccount = payeeAccountOptional.get();
            if(amountTransfer.compareTo(payerAccount.getBalance()) > 0) {
                throw new IllegalArgumentException(Constants.EXCEPTION_MESSAGE_NOT_ENOUGH_BALANCE);
            }
            payerAccount.setBalance(payerAccount.getBalance().subtract(amountTransfer));
            payeeAccount.setBalance(payeeAccount.getBalance().add(amountTransfer));
            accountRepository.save(payerAccount);
            accountRepository.save(payeeAccount);
        }
        return TransactionResponse.builder()
                .message(String.format(Constants.DEFAULT_MESSAGE
                        , payerAccount.getAccountId().toString().replaceAll(Constants.MASK_REGEX, Constants.MASK_CHAR)
                        , Constants.DEFAULT_CURRENCY_CODE
                        , amountTransfer
                        , payeeAccount.getAccountId().toString().replaceAll(Constants.MASK_REGEX, Constants.MASK_CHAR)))
                .balance(Amount.builder()
                        .amount(payerAccount.getBalance())
                        .currencyCode(Constants.DEFAULT_CURRENCY_CODE)
                        .build())
                .build();
    }
}
