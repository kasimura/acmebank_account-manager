package com.acmebank.account_manager.utils;

public class Constants {
    public static final String BALANCE_ENDPOINT = "/balance/{id}";
    public static final String TRANSFER_ENDPOINT = "/transfer";

    public static final String DEFAULT_CURRENCY_CODE = "HKD";
    public static final String DEFAULT_MESSAGE = "You (%s) have been transfered %s $%.2f to %s";

    public static final String MASK_REGEX = ".(?=.{4})";
    public static final String MASK_CHAR = "*";

    public static final String EXCEPTION_MESSAGE_SAME_ACCOUNT_ID = "Payer and payee are same!";
    public static final String EXCEPTION_MESSAGE_INVALID_CURRENCY_CODE = "Invalid currency code!";
    public static final String EXCEPTION_MESSAGE_INVALID_PAYER_OR_PAYEE = "Invalid payer or payee account Id given!";
    public static final String EXCEPTION_MESSAGE_NOT_ENOUGH_BALANCE = "Payer does not have enough to pay!";
}
