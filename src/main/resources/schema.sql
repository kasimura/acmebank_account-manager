CREATE TABLE IF NOT EXISTS account (
    account_id IDENTITY PRIMARY KEY,
    balance DECIMAL(18,3)
);
