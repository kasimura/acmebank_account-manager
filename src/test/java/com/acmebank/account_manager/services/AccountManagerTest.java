package com.acmebank.account_manager.services;

import com.acmebank.account_manager.data.entities.Account;
import com.acmebank.account_manager.data.models.Amount;
import com.acmebank.account_manager.data.models.TransactionRequest;
import com.acmebank.account_manager.data.models.TransactionResponse;
import com.acmebank.account_manager.data.repositories.AccountRepository;
import com.acmebank.account_manager.utils.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.stream.Stream;

@SpringBootTest
public class AccountManagerTest {
    @Autowired
    private AccountRepository repository;

    @Autowired
    private AccountManager accountManager;

    @BeforeEach
    public void assertDependenciesNonNull() {
        Assertions.assertNotNull(repository);
        Assertions.assertNotNull(accountManager);
    }

    @DisplayName("Test account number is not existed in database")
    @Test
    void invalidAccountIdTest() {
        Long accountId = 12345679L;
        Optional<Account> actual = accountManager.getBalance(accountId);
        Assertions.assertEquals(Optional.empty(), actual);
    }

    @DisplayName("Test account number is existed in database")
    @Test
    void validAccountIdTest() {
        Long accountId = 12345678L;
        BigDecimal balance = new BigDecimal("1000000");
        Account account = Account.builder().accountId(accountId).balance(balance).build();
        Optional<Account> actual = accountManager.getBalance(accountId);
        Assertions.assertTrue(actual.isPresent());
        if(actual.isPresent()) {
            Assertions.assertEquals(account.getAccountId(), actual.get().getAccountId());
            Assertions.assertTrue(account.getBalance().compareTo(actual.get().getBalance()) == 0);
        }
    }

    @DisplayName("Test empty transaction request")
    @Test
    void emptyTransactionTest() {
        Long payerAccountId = 12345678L;
        //Long payeeAccountId = 88888888L;
        BigDecimal amount = new BigDecimal("1000");
        String currencyCode = Constants.DEFAULT_CURRENCY_CODE;
        TransactionRequest request = TransactionRequest.builder()
                .payerAccountId(payerAccountId)
                .payeeAccountId(payerAccountId)
                .amount(Amount.builder().amount(amount).currencyCode(currencyCode).build())
                .build();
        Assertions.assertThrows(IllegalArgumentException.class, () -> accountManager.transfer(request), Constants.EXCEPTION_MESSAGE_SAME_ACCOUNT_ID);
    }

    @DisplayName("Test invalid vales of currency code")
    @ParameterizedTest
    @ValueSource(strings = {"USD", "GBP", "ABC"})
    void invalidCurrencyCodeTest(String currencyCode) {
        Long payerAccountId = 12345678L;
        BigDecimal amount = new BigDecimal("1000");
        TransactionRequest request = TransactionRequest.builder()
                .payerAccountId(payerAccountId)
                .payeeAccountId(payerAccountId)
                .amount(Amount.builder().amount(amount).currencyCode(currencyCode).build())
                .build();
        Assertions.assertThrows(IllegalArgumentException.class, () -> accountManager.transfer(request), Constants.EXCEPTION_MESSAGE_INVALID_CURRENCY_CODE);
    }

    private static Stream<Arguments> invalidPayerOrPayeeAccountIds() {
        return Stream.of(
                Arguments.of(12345678L, 88888889L),
                Arguments.of(12345679L, 88888888L),
                Arguments.of(12345679L, 88888889L)
        );
    }

    @DisplayName("Test invalid payer or payee")
    @ParameterizedTest
    @MethodSource("invalidPayerOrPayeeAccountIds")
    void invalidPayerOrPayeeTest(Long payerAccountId, Long payeeAccountId) {
        BigDecimal amount = new BigDecimal("1000");
        TransactionRequest request = TransactionRequest.builder()
                .payerAccountId(payerAccountId)
                .payeeAccountId(payeeAccountId)
                .amount(Amount.builder().amount(amount).currencyCode(Constants.DEFAULT_CURRENCY_CODE).build())
                .build();
        Assertions.assertThrows(IllegalArgumentException.class, () -> accountManager.transfer(request), Constants.EXCEPTION_MESSAGE_INVALID_PAYER_OR_PAYEE);
    }

    @DisplayName(Constants.EXCEPTION_MESSAGE_NOT_ENOUGH_BALANCE)
    @Test
    void notEnoughBalance() {
        Long payerAccountId = 12345678L;
        Long payeeAccountId = 88888888L;
        BigDecimal amount = new BigDecimal("1000001");
        String currencyCode = Constants.DEFAULT_CURRENCY_CODE;
        TransactionRequest request = TransactionRequest.builder()
                .payerAccountId(payerAccountId)
                .payeeAccountId(payeeAccountId)
                .amount(Amount.builder().amount(amount).currencyCode(currencyCode).build())
                .build();
        Assertions.assertThrows(IllegalArgumentException.class, () -> accountManager.transfer(request), Constants.EXCEPTION_MESSAGE_NOT_ENOUGH_BALANCE);
    }

    private static Stream<Arguments> validRequests() {
        BigDecimal payerBalance = new BigDecimal("1000000.00");
        BigDecimal payeeBalance = new BigDecimal("1000000.00");
        BigDecimal txn1Amount = new BigDecimal("1000.00");
        BigDecimal txn2Amount = new BigDecimal("100.25");
        BigDecimal txn3Amount = new BigDecimal("9999.99");

        return Stream.of(
                Arguments.of(12345678L, 88888888L, txn1Amount, payerBalance.subtract(txn1Amount), payeeBalance.add(txn1Amount))
                , Arguments.of(88888888L, 12345678L, txn1Amount, payeeBalance, payerBalance)
                , Arguments.of(12345678L, 88888888L, txn2Amount, payerBalance.subtract(txn2Amount), payeeBalance.add(txn2Amount))
                , Arguments.of(88888888L, 12345678L, txn2Amount, payeeBalance, payerBalance)
                , Arguments.of(12345678L, 88888888L, txn3Amount, payerBalance.subtract(txn3Amount), payeeBalance.add(txn3Amount))
                , Arguments.of(88888888L, 12345678L, txn3Amount, payeeBalance, payerBalance)
        );
    }

    @DisplayName("Test transfer from payer account to payee account")
    @ParameterizedTest
    @MethodSource("validRequests")
    void transferTest(Long payerAccountId, Long payeeAccountId, BigDecimal amount, BigDecimal payerBalance, BigDecimal payeeBalance) {
        TransactionRequest request = TransactionRequest.builder()
                .payerAccountId(payerAccountId)
                .payeeAccountId(payeeAccountId)
                .amount(Amount.builder().amount(amount).currencyCode(Constants.DEFAULT_CURRENCY_CODE).build())
                .build();
        TransactionResponse expected = TransactionResponse.builder()
                .message(String.format(Constants.DEFAULT_MESSAGE
                        , payerAccountId.toString().replaceAll(Constants.MASK_REGEX, Constants.MASK_CHAR)
                        , Constants.DEFAULT_CURRENCY_CODE
                        , amount
                        , payeeAccountId.toString().replaceAll(Constants.MASK_REGEX, Constants.MASK_CHAR)))
                .balance(Amount.builder()
                        .amount(payerBalance)
                        .currencyCode(Constants.DEFAULT_CURRENCY_CODE)
                        .build())
                .build();
        Assertions.assertEquals(expected, accountManager.transfer(request));
        Optional<Account> actual = accountManager.getBalance(payeeAccountId);
        Assertions.assertTrue(actual.isPresent());
        if(actual.isPresent()) {
            Assertions.assertEquals(payeeAccountId, actual.get().getAccountId());
            Assertions.assertTrue(payeeBalance.compareTo(actual.get().getBalance()) == 0);
        }
    }
}
